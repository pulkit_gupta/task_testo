import React, { Component } from 'react';
import { BrowserRouter, Switch, Route,  } from "react-router-dom";

import Header from "./components/Header"
import Home from "./components/Content/Home/Home"
import About from "./components/Content/About";
import "./App.css";
class App extends Component {
  render() {
      return (
        <BrowserRouter>
            <div className="App">
              <Header/>
              <Switch>
                  <Route exact path='/' component={Home}/>
                  <Route exact path='/home' component={Home}/>
                  <Route exact path='/about' component={About}/>
                  <Route exact path='*' component={Home}/>
              </Switch>
          </div>
        </BrowserRouter>
    );
  }
}

export default App;
