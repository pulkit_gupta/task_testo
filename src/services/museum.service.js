import config from "../config";
import {Museum} from "../models/Museum";

export const MuseumService = {
    getAll() {
        return fetch(config.MUSEUM_DATA_URL)
            .then(response => response.json())
            .then(data => data.features.map((feature) => new Museum(feature.properties)));
        // can handle general errors here via error handler
    }
};