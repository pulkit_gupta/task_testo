import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.min.css";
import Moment from "moment";

import "./AppDateRangePicker.css";
import PropTypes from "prop-types";

class AppDateRangePicker extends React.Component {

    static propTypes = {
        startDate: PropTypes.instanceOf(Moment).isRequired,
        duration: PropTypes.string.isRequired,
        handleDateChange: PropTypes.func.isRequired,
        durationChange: PropTypes.func.isRequired
    };

    render() {
        return (
            <div className={"app-date-range-picker"}>
                <span className={"field"}>Datum: </span>
                <div className={"date-picker-widget"}>
                    <DatePicker
                        selected={this.props.startDate}
                        onChange={this.props.handleDateChange}
                        showTimeSelect
                        timeFormat="HH:mm"
                        timeIntervals={15}
                        locale="de"
                        dateFormat="LLL"
                        timeCaption="Zeit"
                    />
                </div>
                <span className={"field"}>Dauer: </span>
                <input className={"duration-field"} type="number" min={0} value={this.props.duration} onChange={this.props.durationChange} />
                <span className={"field"}>stunden</span>
            </div>
        );
    }
}

export default AppDateRangePicker