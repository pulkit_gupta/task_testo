import React from "react";

export const DataField = (props) =>
    <div className={"col"}>
     {props.name}
        <span className={"value-field"}>
              { props.value }
        </span>
    </div>;