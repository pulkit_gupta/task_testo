import React from "react";
import PropTypes from "prop-types";
import "./MuseumListElement.css"
import {Museum} from "../../../../../models/Museum";
import {DataField} from "./DataField";

class MuseumListElement extends React.Component {

    static propTypes = {
      museum: PropTypes.instanceOf(Museum)
    };

    render() {
        const {museum} = this.props;

        const FirstRow = [
            {name: "Öffnungszeiten", value: museum.oeffnungszeiten},
            {name: "Addresse", value: museum.getAddress()},
            {name: "Träger", value: museum.traeger_bezeichnung},
            {name: "Träger Art", value: museum.traeger_art}

        ].map((pair) => <DataField key={pair.name} name={pair.name} value={pair.value}/>);

        const SecondRow = [
            {name: "Telefon", value: museum.telefon},
            {name: "Fax", value: museum.fax},
            {name: "Email", value: museum.email},

        ].map((pair) => <DataField key={pair.name} name={pair.name} value={pair.value}/>);

        return (
          <li className={"museum-list-element"}>
              <div className={"header"}>
                  <span className={"title"}>
                    { museum.bezeichnung }
                  </span>
              </div>
              <div className={"content-data"}>
                  <div className={"row"}>
                      { FirstRow }
                  </div>
                  <div className={"row"}>
                      { SecondRow }

                      <div className={"col"}>
                          Webseite
                          <span className={"value-field"}>
                              <a target="_blank" href={ museum.website }>{museum.website}</a>
                          </span>
                      </div>
                  </div>
              </div>
          </li>
        );
    }
}

export default MuseumListElement;