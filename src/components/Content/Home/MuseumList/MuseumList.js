import React from "react";
import PropTypes from "prop-types";
import MuseumListElement from "./Museum-list-element/MuseumListElement";
import "./MuseumList.css"
import Moment from "moment";

class MuseumList extends React.Component {
    static propTypes = {
        museums: PropTypes.array.isRequired,
        startDate: PropTypes.instanceOf(Moment).isRequired,
        duration: PropTypes.string.isRequired
    };
    applyFilter(museums, chosenTime) {
        return museums.filter((museum) => {
            let x = false;
            museum.openingTimeMap.forEach((map) => {
                if (
                    ( map.months.length === 0 || map.months.indexOf(chosenTime.month()) ) &&
                    map.days.indexOf(chosenTime.day()) !== -1 &&
                    (
                        map.startTime.hour < chosenTime.hour() ||
                        ( map.startTime.hour === chosenTime.hour() && map.startTime.min <= chosenTime.minute() )
                    ) &&
                    (
                        map.endTime.hour - this.props.duration > chosenTime.hour() ||
                        ( map.endTime.hour - this.props.duration === chosenTime.hour() && map.endTime.min >= chosenTime.minute())
                    )
                ) {
                    x = true;
                }
            });
            return x;

        });
    }
    render() {
        const { museums, startDate } = this.props;
        const filteredMuseums = this.applyFilter(museums, startDate);
        let listElements = filteredMuseums.map((museum) => <MuseumListElement museum={museum} key={museum.uuid}/>);

        return (
            <div className={"museum-list"}>
                <div className={"count-display"}>Result count:
                    <span className={"value"}> {filteredMuseums.length} </span></div>
                <ul className={"museum-list-elem"}>{listElements}</ul>
            </div>

        );
    }
}

export default MuseumList;