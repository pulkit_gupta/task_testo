import React from "react";
import MuseumList from "./MuseumList/MuseumList";
import { MuseumService } from "../../../services/museum.service";
import moment from "moment";
import 'moment/locale/de';
import "./Home.css";
import AppDateRangePicker from "./AppDateRangePicker/AppDateRangePicker";

class Home extends React.Component {
    constructor(props) {
        super(props);
        const currentMoment = moment();
        currentMoment.locale('de');
        this.state = {
            startDate: currentMoment,
            duration: '2',
            museums: []
        };

        this.handleDateChange = this.handleDateChange.bind(this);
        this.durationChange = this.durationChange.bind(this);
    }
    componentDidMount() {
        // console.log("config imported is ", config);
        MuseumService.getAll()
            .then(data => {
                this.setState({museums: data});
            });
    }

    handleDateChange(momentObj) {
        console.log('handling dateChange', momentObj);
        this.setState({
            startDate: momentObj
        });
    }

    durationChange(input) {
        this.setState({duration: input.target.value});
    }

    render() {
        return (
            <div className={"home"}>
                <AppDateRangePicker
                    handleDateChange={this.handleDateChange}
                    duration={this.state.duration}
                    durationChange={this.durationChange}
                    startDate={this.state.startDate}/>
                <MuseumList
                    museums={this.state.museums}
                    duration={this.state.duration}
                    startDate={this.state.startDate}/>
            </div>
        )
    }

}

export default Home;