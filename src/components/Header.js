import React from "react";
import { NavLink } from "react-router-dom";
import "./Header.css"
const Header = (props) => (
   <div className="app-header">
       <nav className={"nav-bar"}>
           <ul>
               <li><NavLink className={'link'} activeClassName={'active'} exact to='/'>Startseite</NavLink></li>
               <li><NavLink className={'link'} activeClassName={'active'} exact to='/about'>Über uns</NavLink></li>
           </ul>
       </nav>

   </div>
);

export default Header;