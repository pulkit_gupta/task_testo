import {MuseumHelper} from "./MuseumHelper";

export class Museum {
    uuid;
    kreis_name;
    kreis_schluessel;
    gemeindeverband_name;
    gemeindeverband_schluessel;
    gemeinde_name;
    gemeinde_schluessel;
    gemeindeteil_name;
    gemeindeteil_schluessel;
    strasse_name;
    strasse_schluessel;
    hausnummer;
    hausnummer_zusatz;
    postleitzahl;
    bezeichnung;
    traeger_bezeichnung;
    traeger_art;
    barrierefrei;
    oeffnungszeiten;
    telefon;
    fax;
    email;
    website;

    // can add geometry later on.

    // calculated property
    openingTimeMap = {
        months: [],
        days: [],
        startTime: {hour: 0, min: 0},
        endTime: {hour: 0, min: 0}
    };

    constructor(data) {
        for (const [key, value] of Object.entries(data)) {
            this[key] = value;
        }
        this.openingTimeMap = MuseumHelper.parseOpeningTimeMap(this.oeffnungszeiten);
    }

    getAddress() {
     return `${this.strasse_name} ${this.hausnummer}${this.hausnummer_zusatz ? this.hausnummer_zusatz : ''}, ${this.kreis_name}-${this.postleitzahl}`
    }
}