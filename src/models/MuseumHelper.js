
const GlobalMonths = ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt','Nov','Dez'];
const GlobalDays = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];
const mapper = {'months': GlobalMonths, 'days': GlobalDays};
const Noise = ['Sonderausstellung: ', ',nach Absprache', 'Schaumagazin: '];

// NEED Unit testing

export class MuseumHelper {

    static rangeToNumberArr(arrs, mapperMode) {

        const resultArr = [];
        arrs.forEach((arr) => {
            //not using substr here
            let splitted = arr;
            if (mapperMode === 'months') {
                splitted = arr.split(":")[0].trim(); // removing months :
            }
            splitted = splitted.split("-").map(s => s.trim());
            if (splitted.length === 2) {
                let start = mapper[mapperMode].indexOf(splitted[0]);
                let end = mapper[mapperMode].indexOf(splitted[1]);
                // di -> son which is 2 to 0
                // dec to feb   11 to 2
                if (start > end) {
                    for(let i = start; i <= mapper[mapperMode].length; i++) {
                        resultArr.push(i);
                    }
                    for(let i = end; i >= 0; i--) {
                        resultArr.push(i);
                    }
                } else {
                    for(let i = start; i <= end; i++) {
                        resultArr.push(i);
                    }
                }
            } else if (splitted.length === 1){
                resultArr.push(mapper[mapperMode].indexOf(splitted[0]));
            }
        });

        return resultArr;
    }

    static parseTime(obj, timeRange) {
        const splittedStartTimeRange = timeRange[0].split(":").map((s) => s.trim());
        obj.startTime = {hour: splittedStartTimeRange[0], min: splittedStartTimeRange[1]};

        const splittedEndTimeRange = timeRange[1].split(":").map((s) => s.trim());
        obj.endTime = {hour: splittedEndTimeRange[0], min: splittedEndTimeRange[1]};
    }
    // Unit testing needed
    static parseOpeningTimeMap(openingTimeRaw) {
        if (!openingTimeRaw) {
            return [];
        }
        // prefiltering from noise
        Noise.forEach((noise) => {
            openingTimeRaw = openingTimeRaw.split(noise).map(s => s.trim())[0];
        });

        let slots = openingTimeRaw.split(";").map(s => s.trim());

        const openingMap = [];
        slots.forEach((slot) => {
            let arrayedSlot = slot.split(" ").map(s => s.trim());
            const len = arrayedSlot.length;

            // console.log('arrayed slot is', arrayedSlot);
            const obj = {months: [], days: [], startTime: {}, endTime: {}};
            if (len === 2) {
                obj.days = MuseumHelper.rangeToNumberArr(arrayedSlot[0].split(",").map(s => s.trim()), 'days');
                const timeRange = arrayedSlot[1].split("-");
                MuseumHelper.parseTime(obj, timeRange);
            }
            else if (len === 3) {
                obj.months = MuseumHelper.rangeToNumberArr(arrayedSlot[0].split(",").map(s => s.trim()), 'months');

                obj.days = MuseumHelper.rangeToNumberArr(arrayedSlot[1].split(",").map(s => s.trim()), 'days');

                const timeRange = arrayedSlot[2].split("-");
                MuseumHelper.parseTime(obj, timeRange);

            }
            openingMap.push(obj);

        });

        return openingMap;
    }
}