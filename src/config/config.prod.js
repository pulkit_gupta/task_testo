export const config = {
  env: "prod",
  APP_URL: "myapp.com",
  MUSEUM_DATA_URL: "https://geo.sv.rostock.de/download/opendata/museen/museen.json"
};