export const config = {
  env: "dev",
  APP_URL: "localhost:4000",
  MUSEUM_DATA_URL: "https://geo.sv.rostock.de/download/opendata/museen/museen.json"
};