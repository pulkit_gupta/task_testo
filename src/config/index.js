import { config as devConfig} from "./config.dev"
import { config as prodConfig} from "./config.prod"

console.log('env is ', process.env.REACT_ENV, "Node env = ",process.env.NODE_ENV);
export default process.env.REACT_ENV === 'production' || process.env.NODE_ENV === 'production' ? prodConfig : devConfig;
