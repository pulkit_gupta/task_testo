This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Building and running the app

To run app in dev mode

`npm install  npm start`

To buid the app in respective environment.

`NODE_ENV="prod" npm run build`

## Further improvment

- App can be extended with Redux store